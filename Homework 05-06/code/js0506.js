// Task 1
/*
Напишіть функцію isEmpty(obj), яка повертає true, якщо 
об'єкт не має властивостей, інакше false.
*/

function isEmpty(obj) {
    for (let key in obj) {
        return false
    }
    return true
}

const obj = new Object();

console.log(isEmpty(obj));

obj.myname = 'Kate';

console.log(isEmpty(obj));


// Task 2.1
/*
Разработайте функцию-конструктор, которая будет создавать 
объект Human (человек). Создайте массив объектов и реализуйте 
функцию, которая будет сортировать элементы массива по значению 
свойства Age по возрастанию или по убыванию.
*/

function Human(fname, age) {
    this.fname = fname;
    this.age = age
}

const ivan = new Human('Ivan', 25);
const petro = new Human('Petro', 32);
const pavlo = new Human('Pavlo', 27);
const vlad = new Human('Vlad', 31);

const humans = [ivan, petro, pavlo, vlad];

function sortByAge() {
    return humans.sort((a, b) => a.age < b.age ? 1 : -1) 
}

console.log(sortByAge(Human));


// Task 2.2
/*
Разработайте функцию-конструктор, которая будет создавать 
объект Human (человек), добавьте на свое усмотрение свойства 
и методы в этот объект. Подумайте, какие методы и свойства 
следует сделать уровня экземпляров, в какие уровня функции-конструктора.
*/

function Humans(fname, sname, age) {
    this.fname = fname;
    this.sname = sname;
    this.age = age;
    this.sayHello = function () {
        console.log(`Hello, ${this.fname} ${this.sname}! Your age is ${this.age}.`)
    }
    this.sayHello();
    Humans.CountPeoples++
}

Humans.CountPeoples = 0;

const kate = new Humans('Kate', 'Stepova', 36);
const julia = new Humans('Julia', 'Shevchuk', 33);
const lilya = new Humans('Lilya', 'Tkachuk', 35);
const inna = new Humans('Inna', 'Pirnat', 39);
const olga = new Humans('Olga', 'Ivanenko', 28);

console.log(`Population is ${Humans.CountPeoples}.`)

