let NUM = parseFloat(prompt('Задача 2. Введіть ціле число'));

while (Number.isInteger(NUM) === false) {
    NUM = parseFloat(prompt('Число не ціле. Введіть ціле число'));
}

if (NUM < 5) {
    console.log ('Sorry, no numbers')
} else {
    for (let i = 0; i <= NUM; i++) {
        if (i % 5 === 0) {            
            console.log (i);            
        }
    }
}