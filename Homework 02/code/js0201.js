const star = prompt('Задача 1. Введіть кількість зірочок')

// трикутник

for (let i = 0; i < star; i++) {
    for (let j = 0 + i; j < star; j++) {
        document.write('*');
    }
    document.write('<br>');
}

document.write('<br><br>');


// ромб

for (let i = 1; i < star; i++) {
    for (let j = 1 + i; j < star; j++) {
        document.write('&nbsp;&nbsp;')
    }
    for (let k = star - i; k < star; k++) {
        document.write('*')
    }
    for (let j = 1; j <= i; j++) {
        document.write('*')
    }
    document.write('<br>');
}

for (let i = 1; i < star; i++) {
    for (let j = 1; j <= i; j++) {
        document.write('&nbsp;&nbsp;')
    }
    for (let k = i + 1; k < star; k++) {
        document.write('*')
    }
    for (let j = 1 + i; j < star; j++) {
        document.write('*')
    }
    document.write('<br>');
}

document.write('<br><br>');


// прямокутник

for (let i = 0; i < star; i++) {
    if (i === 0 || i === star - 1) {
        for (let j = 0; j < star; j++) {
            document.write('*');
        }
        document.write('<br>'); 
    } else {
            for (let j = 0; j < star; j++) {
                if (j === 0 || j === star - 1) {
                    document.write('*');
                } else {
                    document.write('&nbsp;&nbsp;')
                }
            }
            document.write('<br>'); 
        }
}
      
document.write('<br><br>');
