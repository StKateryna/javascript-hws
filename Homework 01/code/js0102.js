document.write('<h3>' + 'Задача 2. Математичні операції' + '</h3>');

document.write('<table>');
document.write('<thead>');
document.write('<tr>' + '<th>' + 'Математичний вираз' + '</th>' + '<th>' + 'Результат' + '</th>' + '</tr>');
document.write('</thead>')

document.write('<tr>' + '<td>' + 'x += y - x++ * z' + '</td>');
document.write('<td>');
var x = 6, y = 14, z = 4;
document.write(x += y - x++ * z);
document.write('</td>' + '</tr>');
/* 
x++ = 6 (постфіксний інкремент)
* z = 6 * 4 = 24 (множення)
y - = 14 - 24 = -10 (віднімання)
x += = 6 + (-10) = -4 (присвоєння)
*/

document.write('<tr>' + '<td>' + 'z = --x - y * 5' + '</td>');
document.write('<td>');
x = 6, y = 14, z = 4;
document.write(z = --x - y * 5);
document.write('</td>' + '</tr>');
/* 
--x = 5 (префіксний декремент)
y * 5 = 70 (множення)
5 - 70 = -65 (віднімання)
z = -65 (присвоєння)
*/

document.write('<tr>' + '<td>' + 'y /= x + 5% z' + '</td>');
document.write('<td>');
x = 6, y = 14, z = 4;
document.write(y /= x + 5% z);
document.write('</td>' + '</tr>');
/* 
5%z = 5 / 4 = 1 (залишок від ділення)
x + = 6 + 1 = 7 (додавання)
y /= = 14 / 7 = 2 (присвоєння)
*/

document.write('<tr>' + '<td>' + 'z - x++ + y * 5' + '</td>');
document.write('<td>');
x = 6, y = 14, z = 4;
document.write(z - x++ + y * 5);
document.write('</td>' + '</tr>');
/* 
x++ = 6 (постфіксний інкремент)
y * 5 = 14 * 5 = 70 (множення)
z - = 4 - 6 = -2 (віднімання)
-2 + 70 = 68 (додавання)
*/

document.write('<tr>' + '<td>' + 'x = y - x++ * z' + '</td>');
document.write('<td>');
x = 6, y = 14, z = 4;
document.write(x = y - x++ * z);
document.write('</td>' + '</tr>');
/* 
x++ = 6 (постфіксний інкремент)
* z = 6 * 4 = 24 (множення)
y - = 14 - 24 = -10 (віднімання)
x = -10 (присвоєння)
*/

document.write('</tbody>' + '</table>');