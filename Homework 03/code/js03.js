const style = ['Джаз', 'Блюз'];
document.write (`<div>Початковий вигляд масиву: <span>${style}</span></div>`);
document.write ('<br><hr><br>');

let a = style.push ('Рок-н-рол');
document.write (`<div>Додавання в кінець елементу: <span>${style}</span></div>`);
document.write ('<br><hr><br>');

let b = style.splice (1, 1, 'Класика');
document.write (`<div>Заміна елементу в середині масиву: <span>${style}</span></div>`);
document.write ('<br><hr><br>');

let c = style.splice (0, 1);
document.write (`<div>Видалений елемент: <span>${c}</span>`);
document.write ('<br>');
document.write (`Масив без видаленого елементу: <span>${style}</span></div>`);
document.write ('<br><hr><br>');

let d = style.unshift ('Реп', 'Реггі');
document.write (`<div>Додавання елементів на початок: <span>${style}</span></div>`);